{
  "headers": {
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "cache-control": "no-cache",
    "postman-token": "4e669262-aa4a-47ad-b88b-925584fce61d",
    "host": "localhost:8092",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive",
    "content-length": "0"
  },
  "clientCertificate": null,
  "method": "POST",
  "scheme": "https",
  "queryParams": {},
  "requestUri": "/test",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": null,
  "listenerPath": "/test",
  "relativePath": "/test",
  "localAddress": "/127.0.0.1:8092",
  "uriParams": {},
  "rawRequestUri": "/test",
  "rawRequestPath": "/test",
  "remoteAddress": "/127.0.0.1:50614",
  "requestPath": "/test"
}